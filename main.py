import random

# initialize solved game
a = [[' '] * 3 for i in range(3)]
c = 1
for row in range(len(a)):
  for column in range(len(a)):
    if c >= len(a) ** 2:
      break
    a[row][column] = c
    c += 1

# shuffle
''' for row in range(len(a)):
  for column in range(len(a)):
    r1 = random.randint(0,len(a) - 1)
    r2 = random.randint(0,len(a) - 1)

    buff = a[r1][r2]
    a[r1][r2] = a[row][column]
    a[row][column] = buff
 '''

def solved():
  try:
    for row in range(len(a)):
      for column in range(len(a) - 1):
        if (a[row][column] != a[row][column + 1] - 1):
          print("inner loop")
          return False
      if (a[row + 1][0] != a[row][len(a) - 1]):
        print("outer loop")
        return False
  except TypeError:
    print("error ignored :^)")
  return True


def prettyprint():
  for row in range(len(a)):
    for column in range(len(a)):
      print(str(a[row][column]) + " ", end="")
    print("")

prettyprint()
print(solved())
